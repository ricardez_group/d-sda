# Discrete-time network scheduling and dynamic optimization of batch processes with variable processing times through discrete steepest descent optimization

This repository contains the D-SDA algorithm code for simultaneous scheduling and dynamic optimization including:

- Codes with Pyomo models in "distillation_model", "case_study_1_model.py" and "case_study_2_model.py" 
- Codes required to run computational experiments in "DSDA_experiments.py"

This code was written using the Pyomo modeling environment (http://www.pyomo.org/). The D-SDA code and the functions used to solve and reinitialize subproblems include code snippets from (https://github.com/SECQUOIA/dsda-gdp) and the model_serializer.py code from (https://github.com/IDAES/idaes-pse). 












